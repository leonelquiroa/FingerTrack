folder = '..\video2\';
file = 'video2.mp4';

videoObject = VideoReader(strcat(folder,file));
TotalFrames = videoObject.NumberOfFrames;
vidHeight = videoObject.Height;
vidWidth = videoObject.Width;

for frame = 1 : TotalFrames
    thisFrame = read(videoObject, frame);
    outputFileName = sprintf('Frame %4.4d.png', frame);
    imwrite(thisFrame, strcat(folder,outputFileName));
end
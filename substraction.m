%% input parameters
clear all;
folder = '..\video2\';
file = 'video2.mp4';
FramesPath = strcat(folder,'\Frame 0');
thresholdPercPointer = 0.95;
thresholdMovement = 35;
rangeCrop = 50;
minFrame = 210;
maxFrame = 250;
% transition          234-236
% clean               231-233
%% global variables
videoObject = VideoReader(strcat(folder,file));
vidHeight = videoObject.Height;
vidWidth = videoObject.Width;
% index
IxBw = 1;
IxDiff = 1;
IxCoord = 1;
% storage
DBbw = zeros(vidHeight,vidWidth,(maxFrame-minFrame)+1);       % Original
DBdiff = zeros(vidHeight,vidWidth,(maxFrame-minFrame));       % Differences
DBcoord = cell(1,6);                                          % Coordinates
% radius
minR = 10;
maxR = 20;
% 1 index original frame
% 2 index black and white frame
% 3 type (p,t,n)
% 4 X pos
% 5 Y pos
% 6 radius
%% Detecting circles on pointer frames (P)
for frameNum = minFrame : maxFrame 
    %gray
    DBbw(:,:,IxBw) = convertToGray(FramesPath,frameNum);
    % process de difference
    if(minFrame < frameNum)
        %difference
        Idiff = extractDifferences(DBbw,IxBw);
        DBdiff(:,:,IxDiff) = Idiff;
        % Circle Hough Transform
        [centers, radii] = HoughTransform(Idiff,minR,maxR,'dark',0.95);
        %final storage
        DBcoord{IxCoord,1} = frameNum;                      %frame
        DBcoord{IxCoord,2} = IxBw;                          %index bw
        if(~isempty(centers))
            if(IsOnlyFinger(Idiff,thresholdPercPointer))    
                %F.pointer
                calculated = FingerType(DBcoord,IxCoord,centers,radii);
                %extra validation for pointer
                if (isempty(cell2mat(calculated(2))))
                    DBcoord{IxCoord,3} = 't';
                else
                    DBcoord(IxCoord,3:6) = calculated;
                end
            else
                DBcoord{IxCoord,3} = 't';                   %F.transition
            end
        else
            DBcoord{IxCoord,3} = 'n';                       %F.nothing
        end
        IxCoord = IxCoord + 1;
        IxDiff = IxDiff + 1;
    end
    IxBw = IxBw + 1;
end
%% predict Data for fill the empty data on transition frames (T)
IxCoord = 1;
IxList = 1; 
listFrame = -1;
[~,~,avgR] = avgRadius(DBcoord);
for IxCoord = 1 : size(DBcoord,1)
    if(DBcoord{IxCoord,3} == 't')           %collect the data of the holes
        listFrame(IxList) = IxCoord;
        IxList = IxList + 1;
    elseif(DBcoord{IxCoord,3} == 'p')               %begin and end is (P)
        if(listFrame(1) > 0)
            sizeJump = size(listFrame,2);
            % X calculations
            xPost = DBcoord{IxCoord,4};
            xPrev = DBcoord{IxCoord - sizeJump - 1,4};
            diffX = (xPost - xPrev);
            stepX = diffX/(sizeJump + 1);
            % Y calculations
            yPost = DBcoord{IxCoord,5};
            yPrev = DBcoord{IxCoord - sizeJump - 1,5};
            diffY = (yPost - yPrev);
            stepY = diffY/(sizeJump + 1);
            % fill the holes
            for ix = listFrame
                DBcoord(ix,4:6) = PredictData(DBcoord,ix,stepX,stepY,avgR);
            end
            % reset the indexes
            listFrame = -1;
            IxList = 1; 
        end
    elseif(DBcoord{IxCoord,3} == 'n')               %only the begin is (P)
        if(listFrame(1) > 0)
            sizeJump = size(listFrame,2);
            % X-Y calculations
            coordsX = cell2mat(DBcoord(:,4));
            coordsY = cell2mat(DBcoord(:,5));
            % average step
            stepX = (coordsX(end) - coordsX(1))/size(coordsX,1);
            stepY = (coordsY(end) - coordsY(1))/size(coordsY,1);
            % fill the holes
            for ix = listFrame
                DBcoord(ix,4:6) = PredictData(DBcoord,ix,stepX,stepY,avgR);
            end
            % reset the indexes
            listFrame = -1;
            IxList = 1; 
        end
    end
end
%% detecting circles over cropped area on transition frames (T)
IxBw = 1;
for IxCoord = 1 : size(DBcoord,1)
    if(DBcoord{IxCoord,3} == 't')
        % current
        xCurrent = DBcoord{IxCoord,4};
        yCurrent = DBcoord{IxCoord,5};
        grayCrop = DBbw(...
                    round(yCurrent)-rangeCrop:round(yCurrent)+rangeCrop ...
                    ,round(xCurrent)-rangeCrop:round(xCurrent)+rangeCrop ...
                    ,IxBw+1);
        % prewitt [-1 0 +1]
        [~, Gdir] = imgradient(grayCrop,'prewitt');
        % first attempt
        [centers, radii] = HoughTransform(Gdir,minR,maxR,'dark',0.95);
        % second attempt
        if(isempty(centers) || size(centers,1) > 1)
            [centers, radii] = HoughTransform(Gdir,minR,maxR,'bright',0.95);
        end
        % get real x-y
        if(size(centers,1) == 1) % only one
            DBcoord{IxCoord,4} = (round(xCurrent)-rangeCrop) + centers(1);
            DBcoord{IxCoord,5} = (round(yCurrent)-rangeCrop) + centers(2);
            DBcoord{IxCoord,6} = radii;
        end
    end
    IxBw = IxBw + 1;
end
%% present circles on the images
for IxCoord = 1 : size(DBcoord,1)
    if(DBcoord{IxCoord,3} == 't' || DBcoord{IxCoord,3} == 'p')
        centers = [DBcoord{IxCoord,4}, DBcoord{IxCoord,5}];
        radio = DBcoord{IxCoord,6};
        imshow(DBbw(:,:,DBcoord{IxCoord,2}),[]);
        viscircles(centers, radii,'EdgeColor','b');
    end
end
%% 
function predictCell = PredictData(DBcoord,ix,stepX,stepY,avgR)
    predictCell = cell(1,3);
    predictCell{1,1} = DBcoord{ix-1,4} + stepX;
    predictCell{1,2} = DBcoord{ix-1,5} + stepY;
    predictCell{1,3} = avgR;
end

function Igray = convertToGray(FramesPath,frameNum)
    Irgb = imread(strcat(FramesPath,int2str(frameNum),'.png'));
    Igray = rgb2gray(Irgb);
end

function Idiff = extractDifferences(DBbw,IxBw)
    Idiff = (DBbw(:,:,IxBw) - DBbw(:,:,IxBw-1));
end

function [centers, radii] = HoughTransform(Img,minR,maxR,polarity,thd)
    [centers, radii] = imfindcircles(Img,[minR maxR], ... 
                       'ObjectPolarity',polarity, ...
                       'Sensitivity',thd);
    %viscircles(centers, radii,'EdgeColor','b');
end

function result = IsOnlyFinger(Idiff,threshold)
    perc = sum(Idiff(:)==0) / (size(Idiff,1) * size(Idiff,2));
    result = false;
    if(perc > threshold)
        result = true;
    end
end

function fingerCell = FingerType(DBcoord,IxCoord,center,radii)
    fingerCell = cell(1,4);
    fingerCell{1,1} = 'p';
    if(size(center,1) == 1)                             %only one circle
        fingerCell{1,2} = center(1);
        fingerCell{1,3} = center(2);
        fingerCell{1,4} = radii;    
    else                                                %two circles
        %centers of two points
        center1X = abs(center(1,1) - DBcoord{IxCoord-1,4});
        center1Y = abs(center(1,2) - DBcoord{IxCoord-1,5});
        center2X = abs(center(2,1) - DBcoord{IxCoord-1,4});
        center2Y = abs(center(2,2) - DBcoord{IxCoord-1,5});
        %difference of the two points
        diffX = abs(center1X - center2X);
        diffY = abs(center1Y - center2Y);
        %fprintf('diffX = %16.f and diffY = %16.f\n',diffX,diffY);
        if(diffX > diffY)                           %X is higher
            %disp('X mov');
            if(center1X > center2X)                 %1 is higher than 2
                fingerCell{1,2} = center(1,1);
                fingerCell{1,3} = center(1,2);
                fingerCell{1,4} = radii(1); 
            else                                    %2 is higher than 1
                fingerCell{1,2} = center(2,1);
                fingerCell{1,3} = center(2,2);
                fingerCell{1,4} = radii(2); 
            end
        elseif(diffX < diffY)                           %Y is higher
            %disp('Y mov');
            if(center1Y > center2Y)                 %1 is higher than 2
                fingerCell{1,2} = center(1,1);
                fingerCell{1,3} = center(1,2);
                fingerCell{1,4} = radii(1); 
            else                                    %2 is higher than 1
                fingerCell{1,2} = center(2,1);
                fingerCell{1,3} = center(2,2);
                fingerCell{1,4} = radii(2); 
            end
%        else                    %X and Y moved the same amount (diagonal)
%              disp('X-Y mov');
        end
    end
end

function [minR,maxR,avgR] = avgRadius(DBcoord)
    rangeCircle = cell2mat(DBcoord(:,6));
    minR = floor(min(rangeCircle));
    maxR = ceil(max(rangeCircle));
    avgR = mean(rangeCircle);
end